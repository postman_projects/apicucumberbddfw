Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "pari",
    "job": "leader"
}

Response header date is : 
Sun, 05 May 2024 01:38:13 GMT

Response body is : 
{"name":"pari","job":"leader","id":"826","createdAt":"2024-05-05T01:38:13.816Z"}