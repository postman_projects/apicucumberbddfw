Feature: Trigger the Post API on the basis of InputData


Scenario Outline: Trigger the Post API Request with valid Request parameters
		Given Enter "<Name>" and "<Job>" in post request body
		When Send the post request with data
		Then Validate data_driven_post status code
		And Validate data_driven_post response body parameters
		
Examples:
        |Name |Job |
        |Priya|TL|
        |aaryan|TM|
		
