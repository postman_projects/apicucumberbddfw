Feature: Trigger the post API and Validate the ResponseBody and Response parameters

Scenario: Trigger the API request with valid string RequestBody parameters
		Given Name and Job in RequestBody
		When Send the Request with Payload to the Endpoint
		Then Validate StatusCode
		And Validate ResponseBody parameters
		