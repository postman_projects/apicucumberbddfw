package stepDefinition;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class poststepDefinition {

	File dir_name;
	String requestBody;
	String Endpoint;
	Response response;
	int statuscode;
	ResponseBody res_body;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;

	
	public void beforeScenariopost() throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_post_tc("Post_TC1");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
	}

	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Post_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		statuscode = response.statusCode();
	}

	@Given("Name and Job in RequestBody")
	public void name_and_job_in_request_body() throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_post_tc("Post_TC1");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Test_Case"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

	@When("Send the Request with Payload to the Endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {
		statuscode = response.statusCode();
		res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

	}

	@Then("Validate StatusCode")
	public void validate_status_code() {
		Assert.assertEquals(response.statusCode(), 201);

	}

	@Then("Validate ResponseBody parameters")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
	}
}
